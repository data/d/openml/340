# OpenML dataset: squash-stored

https://www.openml.org/d/340

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: Winna Harvey  
**Source**: [original](http://www.cs.waikato.ac.nz/ml/weka/datasets.html) -   
**Please cite**:   

Squash Harvest Stored

Data source: Winna Harvey
Crop and Food Research, Christchurch, New Zealand

The purpose of the research was to determine the changes taking place in squash fruit during the maturation and ripening so as to pinpoint the best time to give the best quality at the marketplace (Japan). The squash is transported to Japan by refrigerated cargo vessels and takes three to four weeks to reach the market. Evaluations were carried out at a stage representing the quality inspection stage prior to export and also at the stage it would reach on arriving at the market place. 

The original objectives were to determine which pre-harvest variables contribute to good tasting squash after different periods of storage time. This is determined by whether a measure of acceptability found by categorising each squash as either unacceptable, acceptable or excellent.

The fruit in this dataset were stored before being measured, and they have an extra attribute that squash-unstored lacks - the weight of the fruit after storage.

Attribute Information:
1.  site - where fruit is located - enumerated
2.  daf - number of days after flowering - enumerated
3.  fruit - individual number of the fruit (not unique) - enumerated
4.  weight - weight of whole fruit in grams - real
5.  storewt - weight of fruit after storage - real
6.  pene - penetrometer indicates maturity of fruit at harvest - integer
7.  solids_% - a test for dry matter - integer
8.  brix - a refractometer measurement used to indicate sweetness or ripeness of the fruit - integer 
9.  a - the a coordinate of the HunterLab L-a-b notation of colour measurement - integer
10. egdd - the heat accumulation above a base of 8c from emergence of the plant to harvest of the fruit - real
11. fgdd - the heat accumulation above a base of 8c from flowering to harvesting - real
12. groundspot_a - the number indicating colour of skin where the fruit rested on the ground - integer
13. glucose - measured in mg/100g of fresh weight - integer
14. fructose - measured in mg/100g of fresh weight - integer
15. sucrose - measured in mg/100g of fresh weight - integer
16. total - measured in mg/100g of fresh weight - integer
17. glucose+fructose - measured in mg/100g of fresh weight - integer
18. starch - measured in mg/100g of fresh weight - integer
19. sweetness - the mean of eight taste panel scores; out of 1500 - integer
20. flavour - the mean of eight taste panel scores; out of 1500 - integer
21. dry/moist - the mean of eight taste panel scores; out of 1500 - integer
22. fibre - the mean of eight taste panel scores; out of 1500 - integer
23. heat_input_emerg - the amount of heat emergence after harvest - real
24. heat_input_flower - the amount of heat input before flowering - real
25. Acceptability - the acceptability of the fruit - enumerated

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/340) of an [OpenML dataset](https://www.openml.org/d/340). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/340/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/340/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/340/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

